function NhanVien(_taiKhoan, _ten, _email, _password, _ngayLam, _luongCB, _chucVu, _gioLam){
    this.taiKhoan = _taiKhoan;
    this.ten = _ten;
    this.email = _email;
    this.password = _password;
    this.ngayLam = _ngayLam;
    this.luongCB = _luongCB;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function(){
        let tongLuong = 0;
        if(this.chucVu == "Sếp"){
                tongLuong = this.luongCB * 3;
            } else if (this.chucVu == "Trưởng phòng"){
                tongLuong = this.luongCB * 2;
            } else {
                tongLuong = this.luongCB;
            }
        return tongLuong;
    };
    this.xepLoai = function(){
        loaiNhanVien = "";
        if(this.gioLam >= 192){
            loaiNhanVien = "Xuất Sắc";
        } else if(this.gioLam >= 176){
            loaiNhanVien = "Giỏi";
        } else if(this.gioLam >= 160){
            loaiNhanVien = "Khá";
        } else {
            loaiNhanVien = "Trung Bình";
        }
        return loaiNhanVien;
    };
}

function layThongTinTuForm(){
    let taiKhoan = document.getElementById("tknv").value;
    let ten = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let ngayLam = document.getElementById("datepicker").value;
    let luongCB = document.getElementById("luongCB").value * 1;
    let chucVu = document.getElementById("chucVu").value;
    let gioLam = document.getElementById("gioLam").value * 1;
    let nv = new NhanVien(taiKhoan,ten,email,password,ngayLam,luongCB,chucVu,gioLam);
    return nv;
}

function renderDanhSachNhanVien(array){
    var tableDanhSachHHTML = "";
    for(index = 0; index < array.length; index++){
        var tableContent = `<tr>
        <td>${array[index].taiKhoan}</td>
        <td>${array[index].ten}</td>
        <td>${array[index].email}</td>
        <td>${array[index].ngayLam}</td>
        <td>${array[index].chucVu}</td>
        <td>${array[index].tongLuong()}</td>
        <td>${array[index].xepLoai()}</td>
        <td>
        <button onclick="suaNhanVien('${array[index].taiKhoan}')" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
        <button onclick="xoaNhanVien('${array[index].taiKhoan}')" class="btn btn-sm btn-danger">Xoá</button></td>
        </tr>
        `;
        tableDanhSachHHTML+=tableContent;
    }
    document.getElementById("tableDanhSach").innerHTML = tableDanhSachHHTML;
}

function timKiemNhanVien(taiKhoanNhanVien, danhSachNhanVien){
    for (index = 0; index < danhSachNhanVien.length; index++){
        if (taiKhoanNhanVien == danhSachNhanVien[index].taiKhoan){
            return index;
        }   
    } 
    return -1;
}

function showThongTinNhanVien(nhanVien){
    document.getElementById("tknv").value = nhanVien.taiKhoan;
    document.getElementById("name").value = nhanVien.ten;
    document.getElementById("email").value = nhanVien.email;
    document.getElementById("password").value = nhanVien.password;
    document.getElementById("datepicker").value = nhanVien.ngayLam;
    document.getElementById("luongCB").value  = nhanVien.luongCB;
    document.getElementById("chucVu").value = nhanVien.chucVu;
    document.getElementById("gioLam").value = nhanVien.gioLam;
}
function resetForm(){
    document.getElementById("form-nhan-vien").reset(); 
    document.getElementById("tknv").disabled = false;
    document.getElementById("btnCapNhat").style.display = false;
}
