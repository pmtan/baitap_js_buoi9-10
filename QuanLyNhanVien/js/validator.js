
var validator = {
    kiemTraTaiKhoan: function(){
            let taiKhoan = document.getElementById("tknv").value;
            let result = danhSachNhanVien.findIndex(function(nhanVien){
                return nhanVien.taiKhoan == taiKhoan;
            });
            if (result !== -1){
                document.getElementById("tbTKNV").innerText = `Tài khoản đã tồn tại.`;
                document.getElementById("tbTKNV").style.display = "block";
                return false;
            } else {
                document.getElementById("tbTKNV").innerText = ``;}
            let taiKhoanRegex = /^[0-9]{4,6}$/;
            if(taiKhoanRegex.test(taiKhoan)){
                document.getElementById("tbTKNV").innerText = ``;
                return true;
            } else {
                document.getElementById("tbTKNV").innerText = `Tài khoản 4-6 ký số.`;
                document.getElementById("tbTKNV").style.display = "block";
                return false;
            };
    },

    kiemTraTen: function(){
            let nameInput = document.getElementById("name").value;
            let nameRegex = /^[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ]{1,}$/;
            if(nameRegex.test(nameInput)){
                document.getElementById("tbTen").innerText = ``;
                return true;
            } else {
                document.getElementById("tbTen").innerText = `Tên phải là chữ.`;
                document.getElementById("tbTen").style.display = "block";
                return false;
            }
    },
    kiemTraEmail: function(){
            let emailInput = document.getElementById("email").value;
            let emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(emailRegex.test(emailInput)){
                document.getElementById("tbEmail").innerText = ``;
                return true;
            } else {
                document.getElementById("tbEmail").innerText = `Email không hợp lệ.`;
                document.getElementById("tbEmail").style.display = "block";
                return false;
            }
    },
    kiemTraMatKhau: function(){
            let matKhauInput = document.getElementById("password").value;
            let matKhauRegex = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{6,10}$/;
            if(matKhauRegex.test(matKhauInput)){
                document.getElementById("tbMatKhau").innerText = ``;
                return true;
            } else {
                document.getElementById("tbMatKhau").innerText = `Mật khẩu 6-10 ký tự (Ít nhất 1 số, 1 kí tự in hoa, 1 ký tự đặc biệt).`;
                document.getElementById("tbMatKhau").style.display = "block";
                return false;
            }
    },
    kiemTraNgayLam: function(){
            let ngayLamInput = document.getElementById("datepicker").value;
            let ngayLamRegex = /^\d{2}\/\d{2}\/\d{4}$/;
            console.log(ngayLamRegex.test(ngayLamInput));
            if(ngayLamRegex.test(ngayLamInput)){
                document.getElementById("tbNgay").innerText = ``;
                return true;
            } else {
                document.getElementById("tbNgay").innerText = `Ngày làm không hợp lệ.(Định dạng mm/dd/yyyy)`;
                document.getElementById("tbNgay").style.display = "block";
                return false;
            }
    },
    kiemTraLuongCB: function(){
            let inputLuongCB = document.getElementById("luongCB").value;
            if(inputLuongCB < 1000000 || inputLuongCB > 20000000 || inputLuongCB.length == 0){
                document.getElementById("tbLuongCB").innerText = `Lương CB: 1 000 000 - 20 000 000.`;
                document.getElementById("tbLuongCB").style.display = "block";
                return false;
            } else {
                document.getElementById("tbLuongCB").innerText = ``;
                return true;
            }
    },
    kiemTraChucVu: function(){
            let inputChucVu = document.getElementById("chucVu").value;
            if(inputChucVu == ""){
                document.getElementById("tbChucVu").innerText = `Vui lòng chọn chức vụ.`;
                document.getElementById("tbChucVu").style.display = "block";
                return false;
            } else {
                document.getElementById("tbChucVu").innerText = ``;
                return true;
            }
    },
    kiemTraGioLam: function(){
            let inputGioLam = document.getElementById("gioLam").value;
            if(inputGioLam < 80 || inputGioLam > 200 || inputGioLam.length == 0){
                document.getElementById("tbGiolam").innerText = `Số giờ làm 80-200 giờ.`;
                document.getElementById("tbGiolam").style.display = "block";
                return false;
            } else {
                document.getElementById("tbGiolam").innerText = ``;
                return true;
            }
    },
}