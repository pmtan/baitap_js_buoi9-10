function setLocalStorage(data){
    let dataJSON = JSON.stringify(data);
    localStorage.setItem("DanhSachNhanVien", dataJSON);
    renderDanhSachNhanVien(data);
}

function getLocalStorage(){
    let dataLocalStorage =JSON.parse(localStorage.getItem("DanhSachNhanVien"));
    if (dataLocalStorage){
        for(i = 0; i < dataLocalStorage.length; i++){
            let nv = new NhanVien(
                dataLocalStorage[i].taiKhoan,
                dataLocalStorage[i].ten,
                dataLocalStorage[i].email,
                dataLocalStorage[i].password,
                dataLocalStorage[i].ngayLam,
                dataLocalStorage[i].luongCB,
                dataLocalStorage[i].chucVu,
                dataLocalStorage[i].gioLam,
            );
            danhSachNhanVien.push(nv);
        };
    }
    renderDanhSachNhanVien(danhSachNhanVien);
    // return dataLocalStorage;
}
var danhSachNhanVien = [];
getLocalStorage() ;

function themNV(){
    var isValid = validator.kiemTraTaiKhoan()
    & validator.kiemTraTen()
    & validator.kiemTraEmail()
    & validator.kiemTraMatKhau()
    & validator.kiemTraNgayLam() 
    & validator.kiemTraLuongCB() 
    & validator.kiemTraChucVu()
    & validator.kiemTraGioLam()
    if(isValid){
        var newNhanVien = layThongTinTuForm();
        danhSachNhanVien.push(newNhanVien);
        setLocalStorage(danhSachNhanVien);
    }
    document.getElementById("tknv").addEventListener('input',()=>{validator.kiemTraTaiKhoan()});
    document.getElementById("name").addEventListener('input',()=>{validator.kiemTraTen()});
    document.getElementById("email").addEventListener('input',()=>{validator.kiemTraEmail()});
    document.getElementById("password").addEventListener('input',()=>{validator.kiemTraMatKhau()});
    document.getElementById("datepicker").addEventListener('input',()=>{validator.kiemTraNgayLam()});
    document.getElementById("luongCB").addEventListener('input',()=>{validator.kiemTraLuongCB()});
    document.getElementById("chucVu").addEventListener('input',()=>{validator.kiemTraChucVu()});
    document.getElementById("gioLam").addEventListener('input',()=>{validator.kiemTraGioLam()});
}

function suaNhanVien(taiKhoan){
    document.getElementById("tknv").disabled = true;
    document.getElementById("btnThemNV").style.display = "none";
    let index = timKiemNhanVien(taiKhoan, danhSachNhanVien);
    if (index !== -1){
        showThongTinNhanVien(danhSachNhanVien[index]);
    }
}
function capNhatNhanVien(){
    var isValid = validator.kiemTraTen()
    & validator.kiemTraEmail()
    & validator.kiemTraMatKhau()
    & validator.kiemTraNgayLam() 
    & validator.kiemTraLuongCB() 
    & validator.kiemTraChucVu()
    & validator.kiemTraGioLam()
    if(isValid){
        let nhanVienEdited = layThongTinTuForm();
        let index = timKiemNhanVien(nhanVienEdited.taiKhoan, danhSachNhanVien);
        danhSachNhanVien[index] = nhanVienEdited;
        setLocalStorage(danhSachNhanVien);
    }
    document.getElementById("tknv").addEventListener('input',()=>{validator.kiemTraTaiKhoan()});
    document.getElementById("name").addEventListener('input',()=>{validator.kiemTraTen()});
    document.getElementById("email").addEventListener('input',()=>{validator.kiemTraEmail()});
    document.getElementById("password").addEventListener('input',()=>{validator.kiemTraMatKhau()});
    document.getElementById("datepicker").addEventListener('input',()=>{validator.kiemTraNgayLam()});
    document.getElementById("luongCB").addEventListener('input',()=>{validator.kiemTraLuongCB()});
    document.getElementById("chucVu").addEventListener('input',()=>{validator.kiemTraChucVu()});
    document.getElementById("gioLam").addEventListener('input',()=>{validator.kiemTraGioLam()});
}
function xoaNhanVien(taiKhoan){
    var result = confirm(`Bạn có chắc chắn muốn xoá nhân viên ${taiKhoan}?`);
    if (result) {
        let index = timKiemNhanVien(taiKhoan, danhSachNhanVien);
        danhSachNhanVien.splice(index,1);
        setLocalStorage(danhSachNhanVien);
        renderDanhSachNhanVien(danhSachNhanVien);
    }
}
function filterNhanVien(){
    let loaiNhanVien = document.getElementById("searchName").value;
    var danhSachNhanVienFiltered = [];
    for(j = 0; j< danhSachNhanVien.length; j++){
        if(danhSachNhanVien[j].xepLoai() == loaiNhanVien){
            danhSachNhanVienFiltered.push(danhSachNhanVien[j]);
        }
    };
    if(danhSachNhanVienFiltered.length == 0){
        document.getElementById("tableDanhSach").innerHTML =`<tr> Không có kết quả nào được tìm thấy.</tr>`;
    }
    renderDanhSachNhanVien(danhSachNhanVienFiltered);
}
